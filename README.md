# Juju Charm Store Docker Image

This dockerfile checks out and builds the latest Charmstore Client from Canonical.

It also provides a script called storelogin. This will provide an automated login to the charmstore using Expect to mimic command line actions if you don't use 2FA.

## Environment variables

EMAIL: your Ubuntu One email address

PASSWORD: your Ubuntu One password

## Using this image

You can pull the image from

    registry.gitlab.com/spiculedata/juju/charmstore-docker
