FROM golang:stretch

RUN go get github.com/juju/charmstore-client | true
RUN cd $GOPATH/src/github.com/juju/charmstore-client && make deps && make install

RUN apt update && apt install -y expect

ADD ./storelogin /usr/bin/
ADD ./charm-push.sh /usr/bin/

RUN chmod +x /usr/bin/storelogin
RUN chmod +x /usr/bin/charm-push.sh
