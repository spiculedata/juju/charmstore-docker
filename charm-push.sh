#!/bin/bash

charm_ref="$(/go/bin/charm push $PWD/charms/builds/$CHARM_NAME $CHARM_LOCATION | grep -m 1 url | awk '{ print $2 }')"
/go/bin/charm release $charm_ref --channel $CHANNEL $RESOURCES

if [ -z "$PLAN_NAME" ]
then
      echo "No Plan Attached"
else
      /go/bin/charm attach-plan $charm_ref $PLAN_NAME
fi